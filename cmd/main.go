package main

import (
	"fmt"
	"os"
	"strings"
	"unicode"
)

// Returns a reversed string
func reverseString(s string) string {
	var result string
	for _, c := range s {
		result = string(c) + result
	}

	return result
}

// Checks if string is a palindrome
func isPalindrome(s string) bool {
	return s == reverseString(s)
}

func main() {
	// Join input arguments
	rawInput := strings.Join(os.Args[1:], "")
	var input string
	for _, c := range rawInput {
		// Filter non letters
		if unicode.IsLetter(c) {
			// Append character in lower case
			input += string(unicode.ToLower(c))
		}
	}

	// Print results
	if isPalindrome(input) {
		fmt.Println("It's a palindrome.")
	} else {
		fmt.Println("It's not a palindrome.")
	}
	fmt.Println("Your input was '"+input+"'.", "The reversed input is '"+reverseString(input)+"'.")
}
